# Setup  
1. Unzip bin/perscules  
2. Run perscules.exe  
  
# Other Info
1. When a browser tab opens up to login, login, and then paste the authorization code it gives you into the program.  
2. Large folders take a longer time to upload. (Obviously.)  

![Perscules GUI](https://adam.got-lost-in.space/1kig7j.png)