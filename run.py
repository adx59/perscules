#!/usr/bin/env python
import wx
import src.gui.gui as gui

if __name__ == '__main__':
    app = wx.App()
    frame = gui.MainGuiFrame(
        None,
        title="Perscules",
        style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER,
        size=(280, 310)
    )
    frame.Show()
    app.MainLoop()

