#!/usr/bin/env python
import requests
import webbrowser

def get_code(client_id, redirect_uri, scope):
    """Gets an auth code url"""
    auth_url = ("https://accounts.google.com/o/oauth2/v2/auth?"
                f"client_id={client_id}&redirect_uri={redirect_uri}"
                f"&response_type=code&scope={scope}")

    return auth_url

def get_token(code, redirect_uri, client_id, client_secret):
    """Makes a POST request to Google OAuth for an access token."""
    endpoint = "https://www.googleapis.com/oauth2/v4/token"
    body = {
        "code": code,
        "redirect_uri": redirect_uri,
        "client_id": client_id,
        "client_secret": client_secret,
        "grant_type": "authorization_code"
    }

    res = requests.post(endpoint, data=body)
    json_res = res.json()

    if "access_token" not in json_res:
        return False
    else:
        return json_res["access_token"]

def oauth(client_id, client_secret, redirect_uri, scope):
    """The whole oauth jam."""
    code = get_code(client_id, redirect_uri, scope)
    token = get_token(code["code"], redirect_uri, client_id, client_secret)

    return token
