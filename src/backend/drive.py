#!/usr/bin/env python
import requests
import json

def create_folder(name, token, parents=[]):
    endpoint = 'https://www.googleapis.com/drive/v3/files'
    data = {
        'name': name,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': parents
    }
    headers = {
        'Authorization': f'Bearer {token}'
    }

    res = requests.post(endpoint, headers=headers, json=data)

    return res.json()

def upload_file(name, fdata, token, parents=[]):
    endpoint = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart'
    data = {
        'metadata': ( 
            '',
            json.dumps({
                'name': name,
                'mimeType': 'application/octet-stream',
                'parents': parents
            }),
            'application/json'
        ),
        'file': (
            name,
            fdata.read(),
            'application/octet-stream'
        )
    }
    headers = {
        'Content-Length': str(len(fdata.read())),
        'Authorization': f'Bearer {token}'
    }

    res = requests.post(endpoint, headers=headers, files=data)

    return res.json()
