#!/usr/bin/env python
import os
import src.backend.drive as drive

def backup_folder(folder_name, folder, parents, token, gauge=None, finish_handler=None):
    if not os.path.isdir(folder):
        if finish_handler:
            finish_handler(success=False)
        return

    folder_res = drive.create_folder(folder_name, token, parents=([parents[0]] if parents else []))

    parents.insert(0, folder_res['id'])
    file_ids = {}

    for _file in os.listdir(folder):
        try:
            with open(f"{folder}/{_file}", 'rb') as fdata:
                file_res = drive.upload_file(_file, fdata, token, parents=[parents[0]])
                file_ids[f"{folder}/{_file}"] = file_res["id"]
                if gauge:
                    gauge.Pulse()
        except PermissionError:  # file is a directory, use recursion
            ids = backup_folder(_file, f"{folder}/{_file}", parents.copy(), token)
            file_ids.update(ids)
            if gauge:
                gauge.Pulse()

    if gauge:
        gauge.SetValue(100)

    if finish_handler:
        finish_handler(res=folder_res)

    return file_ids
    
