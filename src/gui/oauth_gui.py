#!/usr/bin/env python
import webbrowser
import wx
import wx.lib.newevent

import src.backend.oauth as oauth

class OAuthGuiFrame(wx.Frame):
    REDIRECT_URI = "http://adx59.dx.am/misc/oauth.php"
    CLIENT_ID = "1024771443431-7ilshhv3nbjh198rgauuipl55i36ujai.apps.googleusercontent.com"
    CLIENT_SECRET = "6OSxJh1oD795zps8HTH7R4tV"
    SCOPE = "https://www.googleapis.com/auth/drive"

    def __init__(self, *args, **kwargs):
        self.oauth_finish_handler = kwargs.pop("finish_handler")
        super(OAuthGuiFrame, self).__init__(*args, **kwargs)

        self.panel = wx.Panel(self)
        ico = wx.Icon('favicon.ico', wx.BITMAP_TYPE_ICO)
        self.SetIcon(ico)

        self.render_gui()
        self.open_oauth_page()

    def open_oauth_page(self):
        url = oauth.get_code(self.CLIENT_ID, self.REDIRECT_URI, self.SCOPE)
        webbrowser.open(url)

    def render_gui(self):
        text1 = wx.StaticText(self.panel, label="Authorization Code:", pos=(35,10))
        self.code_input = wx.TextCtrl(self.panel, pos=(33,30))
        submit_button = wx.Button(self.panel, label="Submit", pos=(42, 70))
        submit_button.Bind(wx.EVT_BUTTON, self.onsubmit)

    def onsubmit(self, event):
        code = self.code_input.GetValue()
        token = oauth.get_token(code, self.REDIRECT_URI, self.CLIENT_ID, self.CLIENT_SECRET)

        if not token:
            wx.MessageBox(
                "That is an invalid authentication code. Please try again.",
                "Error",
                wx.OK|wx.ICON_ERROR|wx.CENTRE
            )

        else:
            # create event
            completed_event, COMPLETED_EVT_EVENT = wx.lib.newevent.NewEvent()
            self.Bind(COMPLETED_EVT_EVENT, self.oauth_finish_handler)

            evt = completed_event(token=token)
            wx.PostEvent(self, evt)

            self.Destroy()
