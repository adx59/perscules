#!/usr/bin/env python
import webbrowser
import threading
import wx

import src.backend.backup as backup
from .oauth_gui import OAuthGuiFrame

class MainGuiFrame(wx.Frame):
    def __init__(self, *args, **kw):
        super(MainGuiFrame, self).__init__(*args, **kw)

        self.panel = wx.Panel(self)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        ico = wx.Icon('favicon.ico', wx.BITMAP_TYPE_ICO)
        self.SetIcon(ico)

        self.render_gui()

    
    def render_gui(self):
        text1 = wx.StaticText(
            self.panel,
            label="Folder to Backup",
            pos=(80, 15), 
        )

        self.to_backup = wx.TextCtrl(
            self.panel, 
            pos=(72, 40), 
            
        )
        text2 = wx.StaticText(
            self.panel, 
            label="Google Drive Folder Name", 
            pos=(60, 80), 
            
        )
        self.folder_name = wx.TextCtrl(
            self.panel, 
            pos=(72, 105), 
        )

        self.submit_button = wx.Button(
            self.panel,
            label="Upload",
            pos=(80, 155),
        )
        self.submit_button.Bind(wx.EVT_BUTTON, self.onsubmit)

    def onsubmit(self, event):
        oauth_frame = OAuthGuiFrame(
            None,
            title="OAuth",
            style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER,
            size=(200, 300),
            finish_handler=self.on_oauth_finish,
        )

        oauth_frame.Show()

    def on_oauth_finish(self, event):
        token = event.token

        progress_gauge = wx.Gauge(self.panel, pos=(35, 200))

        backup_thread = threading.Thread(
            target=backup.backup_folder, 
            args = [
                self.folder_name.GetValue(),
                self.to_backup.GetValue(),
                [],
                token, 
            ],
            kwargs = {
                'gauge': progress_gauge,
                'finish_handler': self.on_upload_finish
            }
        )
        backup_thread.start()

    def on_upload_finish(self, success=True, res=''):
        if not success:
            wx.MessageBox(
                "An error occured. Please re-check your folder location.",
                "Error",
                wx.OK|wx.ICON_ERROR|wx.CENTRE
            )
            self.Destroy()
        else:
            wx.MessageBox(
                f"Finished backing up {self.to_backup.GetValue()}!",
                "Complete!",
                wx.OK|wx.ICON_INFORMATION|wx.CENTRE
            )

            self.Destroy()

